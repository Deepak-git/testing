const express = require('express');
const router = express.Router();
const userController=require("../../controller/userController")
var auth = require('../../middleWare/auth');


const validation = require('../../middleWare/validation');
  
  
  
  /**
 * @swagger
 * /api/v1/user/signUp:
 *   post:
 *     tags:
 *       - USER
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: first name
 *         in: formData
 *         required: true
 *       - name: middleName
 *         description: middle Name
 *         in: formData
 *         required: true
 *       - name: lastName
 *         description: last Name
 *         in: formData
 *         required: true
 *       - name: password
 *         description: password
 *         in: formData
 *         required: true
 *       - name: state
 *         description: state
 *         in: formData
 *         required: true
 *       - name: mobileNumber
 *         description: mobile Number
 *         in: formData
 *         required: true
 *       - name: countryCode
 *         description: country Code
 *         in: formData
 *         required: true
 *       - name: gender
 *         description: gender
 *         in: formData
 *         required: true 
 *       - name: questionId
 *         description: questionId
 *         in: formData
 *         required: true       
 *       - name: answer
 *         description: answer
 *         in: formData
 *         required: true 
 *       - name: userName
 *         description: userName
 *         in: formData
 *         required: true  
 *       - name: emailId
 *         description: emailId
 *         in: formData
 *         required: true  
 *       - name: countryCode
 *         description: countryCode
 *         in: formData
 *         required: true  
 *       - name: pin
 *         description: pin
 *         in: formData
 *         required: true  
 *     responses:
 *       200:
 *         description: SignUp successfully
 *       404:
 *         description: Invalid credentials
 *       500:
 *         description: Internal Server Error
 */
  router.post("/signUp", userController.signUp)
/**
 * @swagger
 * /api/v1/user/logIn:
 *   post:
 *     tags:
 *       - USER
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: mobileNumber
 *         description: mobileNumber
 *         in: formData
 *         required: true
 *       - name: password
 *         description: password
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Login successfully
 *       404:
 *         description: Invalid credentials
 *       500:
 *         description: Internal Server Error
 */

  router.post("/otpSent",userController.otpSent)

  router.post("/verifyOtp",userController.verifyOtp)


  router.post("/forgotPassword",userController.forgotPassword)

  router.post("/resetPassword", auth.verifyToken,userController.resetPassword)

  router.post("/showAgentList", auth.verifyToken,userController.showAgentList)

  router.post("/agentDetalis", auth.verifyToken,userController.agentDetalis)

  router.post("/blockAgent", auth.verifyToken,userController.blockAgent)

  router.post("/sendMoney", auth.verifyToken,userController.sendMoney)

  router.post("/withdrawalMoney", auth.verifyToken,userController.withdrawalMoney)

  router.post("/qrScanToPay", auth.verifyToken,userController.qrScanToPay)
    
  router.post("/addMoneySuperAgent", auth.verifyToken,userController.addMoneySuperAgent)
  router.post("/editsettingInformation", auth.verifyToken,userController.editsettingInformation)




/**
 * @swagger
 * /api/v1/user/kycSubmitDetails:
 *   post:
 *     tags:
 *       - USER
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - name: VoterID_Name
 *         description: voter id name 
 *         in: formData
 *         required: true
 *       - name: VoterID_Number
 *         description: voter id number
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Submit successfully
 *       404:
 *         description: Data not found
 *       500:
 *         description: Internal Server Error
 */

  router.post("/kycSubmitDetails", auth.verifyToken,userController.kycSubmitDetails)

  router.get("/blockAgentList", auth.verifyToken,userController.blockAgentList)

  router.post("/postAdd",userController.postAdd)

  router.post("/addCard",auth.verifyToken,userController.addCard)

    
  
module.exports=router
