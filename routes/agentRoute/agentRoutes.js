const express = require('express');
const router = express.Router();
const agentController=require("../../controller/agentController")
var auth = require('../../middleWare/auth');
const validation = require('../../middleWare/validation');
  
console.log()

/**
 * @swagger
 * /api/v1/agent/logIn:
 *   post:
 *     tags:
 *       - AGENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: agentId
 *         description: agentId
 *         in: formData
 *         required: true
 *       - name: mobileNumber
 *         description: mobileNumber
 *         in: formData
 *         required: true
 *       - name: password
 *         description: password
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Login successfully
 *       404:
 *         description: Invalid credentials
 *       500:
 *         description: Internal Server Error
 */


router.post('/logInAgent',agentController.logInAgent)
/**
 * @swagger
 * /api/v1/agent/forgotPassword:
 *   post:
 *     tags:
 *       - AGENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: agentId
 *         description: agentId
 *         in: formData
 *         required: true
 *       - name: mobileNumber
 *         description: mobileNumber
 *         in: formData
 *         required: true
 *       - name: password
 *         description: password
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Login successfully
 *       404:
 *         description: Invalid credentials
 *       500:
 *         description: Internal Server Error
 */
router.post('/forgotPassword',agentController.forgotPassword)
router.post('/getStaticContent',agentController.getStaticContent)
router.post('/addAgentBySuperAgent',auth.verifyToken,agentController.addAgentBySuperAgent)

router.post('/verifyOtp',agentController.verifyOtp)
router.post('/resetPassword',agentController.resetPassword)
router.post('/createPin',auth.verifyToken,agentController.createPin)
router.post('/sendRequestToSuperAgentForAddMoneyByAgent',auth.verifyToken,agentController.sendRequestToSuperAgentForAddMoneyByAgent)
router.post('/DetailsConfirmToAddMoneyRequest',auth.verifyToken,agentController.DetailsConfirmToAddMoneyRequest)
router.post('/blockCustomer',agentController.blockCustomer)
router.post('/agentQrScanToPay',agentController.agentQrScanToPay)
router.post('/activeBlockCustomer',agentController.activeBlockCustomer)



router.post('/DetailsConfirmToWithdrawMoneyRequest',auth.verifyToken,agentController.DetailsConfirmToWithdrawMoneyRequest)


//=============================agent API's=============================
router.post('/approveAddMoneyRequestOfAgent',auth.verifyToken,agentController.approveAddMoneyRequestOfAgent)
router.post('/rejectAddMoneyRequestOfAgent',auth.verifyToken,agentController.rejectAddMoneyRequestOfAgent)
router.post('/transferMoneyBySuperAdminToAgent',auth.verifyToken,agentController.transferMoneyBySuperAdminToAgent)

//===========================================QR Code Payments================================================
router.post('/qrCodeSendBySuperAgentToAgent',auth.verifyToken,agentController.qrCodeSendBySuperAgentToAgent)
router.post('/receiveMoneyQrCodeBySuperAgent',auth.verifyToken,agentController.receiveMoneyQrCodeBySuperAgent)
router.post('/qrCodeConfirmWithPinPassword',auth.verifyToken,agentController.qrCodeConfirmWithPinPassword)
router.post('/exchangeMoneyBySuperAgent',auth.verifyToken,agentController.exchangeMoneyBySuperAgent)
router.post('/checkBalance',auth.verifyToken,agentController.checkBalance)
router.post('/blockAgentBySuperAgent',auth.verifyToken,agentController.blockAgentBySuperAgent)
router.post('/supportMessageBySuperAgentToAdmin',auth.verifyToken,agentController.supportMessageBySuperAgentToAdmin)

















module.exports = router

	
	
	
