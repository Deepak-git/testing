const express = require('express')
const bodyParser = require('body-parser')
const config = require('./config/config.js');
const cors = require('cors')
const db = require('./dbConnection/mongodb');
const index=require('./routes/indexRoutes')
const app = express();
const path = require('path');
//var expressValidator = require('express-validator');
const morgan = require('morgan');

const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

var swaggerDefinition = {
  info: {
    title: 'PixalApp',
    version: '2.0.0',
    description: 'Documentation of Pixal Application',
  },
  host: `${global.gConfig.swaggerURL}`,
  basePath: '/',
};
var options = {
  swaggerDefinition: swaggerDefinition,
  apis: ['./routes/*/*.js']
};

var swaggerSpec = swaggerJSDoc(options);

app.get('/swagger.json', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);


});

// initialize swagger-jsdoc
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// app.get('/api/:name',(req,res)=>{
//   res.send({responseMessage: "Hello" + "" +  req.params.name})
// })



//app.use( expressValidator())
app.use(bodyParser.urlencoded({extended:true,limit:'50mb'}));
app.use(bodyParser.json({limit:'50mb'}));
app.use(morgan('dev'))

app.use(cors());
app.use('/api/v1', index);

app.use('/', express.static(path.join(__dirname, 'public')));
app.listen(global.gConfig.node_port, function () {
    console.log(' Server is listening on ', global.gConfig.node_port);
});

